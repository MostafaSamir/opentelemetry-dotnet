# OpenTelemetry Roll Dice Application with Docker

## Description
This application demonstrates the usage of OpenTelemetry for tracing and metrics in a .NET web application, deployed using Docker containers. It includes functionality to roll a dice, handle HTTP requests, and send greetings. Prometheus is used for monitoring metrics, and OpenTelemetry Collector is used for collecting and exporting telemetry data.

## Prerequisites
- Docker installed on your machine.

## Installation
1. Clone the repository.
2. Build the Docker image: `docker build -t roll-dice-app .`
3. Run the Docker container: `docker run -d -p 8080:80 roll-dice-app`

## Usage
- Access the application at `http://localhost:8080`.
- Access the Prometheus metrics endpoint at `http://localhost:9090/metrics`.

## Docker Compose
To deploy the application along with monitoring and tracing components using Docker Compose:

1. Clone the repository.
2. Navigate to the repository directory.
3. Run `docker-compose up -d` to start the containers in detached mode.
4. Access the application at `http://localhost:8080`.
5. Access Prometheus at `http://localhost:9090`.
6. Access Jaeger at `http://localhost:16686`.
7. Access Zipkin at `http://localhost:9411`.
8. Access Seq at `http://localhost:5341`.

## Configuration Files
- `prometheus.yaml`: Configuration file for Prometheus.
- `otel-collector-config.yaml`: Configuration file for OpenTelemetry Collector.
- `docker-compose.yaml`: Docker Compose configuration file for deploying the application stack.

## Contributing
Contributions are welcome! Please feel free to submit bug reports, feature requests, or pull requests to the repository.

## Contact
For any inquiries, please contact [mostafasamir355@gmail.com](mailto:mostafasamir355@gmail.com).

## Acknowledgements
- This project utilizes OpenTelemetry for tracing and metrics instrumentation.
- Special thanks to the contributors and maintainers of OpenTelemetry.
- Prometheus, Jaeger, Zipkin, Seq, and Docker Compose are used for monitoring, tracing, and container orchestration.
