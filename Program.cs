using System.Diagnostics.Metrics;
using System.Diagnostics;
using System.Globalization;
using System.Xml.Linq;
using OpenTelemetry.Trace;
using OpenTelemetry.Metrics;
using OpenTelemetry.Resources;
using OpenTelemetry.Logs;


AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
var builder = WebApplication.CreateBuilder(args);
var httpClient = new HttpClient();

const string serviceName = "roll-dice";

// Configure logging
builder.Logging.AddOpenTelemetry(builder =>
{
    builder.IncludeFormattedMessage = true;
    builder.IncludeScopes = true;
    builder.ParseStateValues = true;
    builder.AddOtlpExporter(options => options.Endpoint = new Uri("http://localhost:4317"));
});
var greeterMeter = new Meter("OtPrGrYa.Example", "1.0.0");
var meter = new Meter("MyApplicationMetrics");
var activitySource = new ActivitySource("MyApplicationActivitySource");

// Configure Tracing and Metrics 
builder.Services.AddOpenTelemetry()
      .ConfigureResource(resource => resource.AddService(serviceName))
      .WithTracing(tracing => tracing
          .AddAspNetCoreInstrumentation()
          .AddHttpClientInstrumentation()
          .AddSource("MyApplicationActivitySource")
          .AddOtlpExporter(options => options.Endpoint = new Uri("http://localhost:4317")))
      .WithMetrics(metrics => metrics
          .AddAspNetCoreInstrumentation()
          .AddHttpClientInstrumentation()
          .AddMeter("MyApplicationMetrics")
          .AddMeter(greeterMeter.Name)
          // Metrics provides by ASP.NET Core in .NET 8
          .AddMeter("Microsoft.AspNetCore.Server.Kestrel")
          .AddMeter("Microsoft.AspNetCore.Hosting")
          .AddOtlpExporter(options => options.Endpoint = new Uri("http://localhost:4317")));

var app = builder.Build();
var logger = app.Logger;

int RollDice()
{
    return Random.Shared.Next(1, 7);
}

var requestCounter = meter.CreateCounter<int>("compute_requests");

async Task<string> HandleRollDiceAsync(string? player)
{
    var result = RollDice();

    if (string.IsNullOrEmpty(player))
    {
        logger?.LogInformation("Anonymous player is rolling the dice: {result}", result);
    }
    else
    {
        logger?.LogInformation("{player} is rolling the dice: {result}", player, result);
    }

    // The sampleActivity is automatically linked to the parent activity (the one from
    // ASP.NET Core in this case).
    // You can get the current activity using Activity.Current.
    using (var sampleActivity = activitySource?.StartActivity("Sample", ActivityKind.Server))
    {
        // note that "sampleActivity" can be null here if nobody listen events generated
        // by the "SampleActivitySource" activity source.
        sampleActivity?.AddTag("Name", player);
        sampleActivity?.AddBaggage("SampleContext", player);

        // Simulate a long running operation
        await Task.Delay(1000);
    }

    requestCounter.Add(1);
    await httpClient.GetStringAsync("https://www.google.com/");

    return result.ToString(CultureInfo.InvariantCulture);
}

async Task<IResult> HandleMainPageAsync() {
    requestCounter.Add(1);

    using (var activity = activitySource.StartActivity("Get data"))
    {
        // Add data the the activity
        // You can see these data in Zipkin
        activity?.AddTag("sample", "value");

        // Http calls are tracked by AddHttpClientInstrumentation
        var str1 = await httpClient.GetStringAsync("https://apple.com");
        var str2 = await httpClient.GetStringAsync("https://www.microsoft.com");

        logger.LogInformation("Response1 length: {Length}", str1.Length);
        logger.LogInformation("Response2 length: {Length}", str2.Length);
    }

    return Results.Ok();
}

var countGreetings = greeterMeter.CreateCounter<int>("greetings", description: "Counts the number of greetings");
string SendGreeting(ILogger<Program> logger)
{
    // Create a new Activity scoped to the method
    using var activity = activitySource.StartActivity("GreeterActivity");

    // Log a message
    logger.LogInformation("Sending greeting");

    // Increment the custom counter
    countGreetings.Add(1);

    // Add a tag to the Activity
    activity?.SetTag("greeting", "Hello World!");

    return "Hello World!";
}

app.MapGet("/greet", SendGreeting);
app.MapGet("/rolldice/{player?}", HandleRollDiceAsync);
app.MapGet("/", HandleMainPageAsync);

app.Run();